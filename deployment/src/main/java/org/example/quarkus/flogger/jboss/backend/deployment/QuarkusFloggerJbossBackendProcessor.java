package org.example.quarkus.flogger.jboss.backend.deployment;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

class QuarkusFloggerJbossBackendProcessor {

    private static final String FEATURE = "quarkus-flogger-jboss-backend";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }

}
