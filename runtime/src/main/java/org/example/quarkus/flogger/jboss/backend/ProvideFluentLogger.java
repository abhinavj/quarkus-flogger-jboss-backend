package org.example.quarkus.flogger.jboss.backend;

import com.google.common.flogger.FluentLogger;
import com.google.common.flogger.backend.Platform;
import com.google.common.flogger.backend.system.StackBasedCallerFinder;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

@Dependent
public class ProvideFluentLogger {

    @Produces
    public FluentLogger provideFluentLogger(){
        return FluentLogger.forEnclosingClass();
    }
}