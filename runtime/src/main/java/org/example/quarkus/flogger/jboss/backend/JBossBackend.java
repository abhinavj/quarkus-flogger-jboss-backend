package org.example.quarkus.flogger.jboss.backend;

import com.google.common.flogger.LogSite;
import com.google.common.flogger.MetadataKey;
import com.google.common.flogger.backend.*;
import com.google.common.flogger.backend.system.SimpleLogRecord;
import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class JBossBackend extends LoggerBackend {

    private final Logger logger;

    static Level toJBossLogLevel(java.util.logging.Level level) {
        int logLevel = level.intValue();
        if (logLevel >= java.util.logging.Level.SEVERE.intValue()) {
            return Level.ERROR;
        } else if (logLevel >= java.util.logging.Level.WARNING.intValue()) {
            return Level.WARN;
        } else if (logLevel >= java.util.logging.Level.INFO.intValue()) {
            return Level.INFO;
        } else if (logLevel >= java.util.logging.Level.FINE.intValue()) {
            return Level.DEBUG;
        }
        return Level.FATAL;
    }

    public JBossBackend(Logger logger) {
        this.logger = logger;
    }

    @Override
    public String getLoggerName() {
        return logger.getName();
    }

    @Override
    public boolean isLoggable(java.util.logging.Level level) {
        return  logger.isEnabled(toJBossLogLevel(level));
    }

    @Override
    public void log(LogData data) {
        Map<String, Object> out = new HashMap<String, Object>();
        SimpleMessageFormatter.format(data, new JBossSimpleLogHandler(out));

        out.put("timestampNanos", data.getTimestampNanos());

        if (data.getLogSite() != null) {
            LogSite logSite = data.getLogSite();
            if (logSite.getClassName() != null)  out.put("className", logSite.getClassName());
            if (logSite.getFileName() != null)   out.put("fileName", logSite.getFileName());
                                                 out.put("lineNumber", logSite.getLineNumber());
            if (logSite.getMethodName() != null) out.put("methodName", logSite.getMethodName());
        }

        if (data.getMetadata() != null) {
            Metadata metadata = data.getMetadata();
            Map<String, List<Object>> repeated = new HashMap<>();
            for (int i = 0; i < metadata.size(); i++) {
                MetadataKey<?> key = metadata.getKey(i);
                String label = key.getLabel();
                Object value = metadata.getValue(i);
                if (value == null) continue;
                if (value instanceof Tags) {
                    Tags tags = (Tags) value;
                    tags.emitAll(new TagsKeyValueHandler(label, out));
                } else {
                    if (key.canRepeat()) {
                        repeated.computeIfAbsent(label, argument -> new ArrayList<>()).add(value);
                    } else {
                        out.put(label, value);
                    }
                }
            }

            repeated.forEach(out::put);
        }
      logger.log(toJBossLogLevel(data.getLevel()),out);

    }

    @Override
    public void handleError(RuntimeException error, LogData badData) {
        Map<String, Object> out = new HashMap<String, Object>();
        out.put("level", Level.ERROR.name());
        out.put("message", SimpleLogRecord.error(error, badData));
        logger.log(Level.ERROR, out);
    }

}
