package org.example.quarkus.flogger.jboss.backend.test;

import com.google.common.flogger.FluentLogger;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import io.quarkus.test.QuarkusUnitTest;

class QuarkusFloggerJbossBackendTest {

    @RegisterExtension
    static final QuarkusUnitTest unitTest = new QuarkusUnitTest() // Start unit test with your extension loaded
        .setArchiveProducer(() -> ShrinkWrap.create(JavaArchive.class));

    @Test
    public void test() {
        // Write your tests here - see the testing extension guide https://quarkus.io/guides/writing-extensions#testing-extensions for more information
        Assertions.fail("Add some assertions to " + getClass().getName());
    }

    @Test
    public void test2(){
        FluentLogger logger = FluentLogger.forEnclosingClass();
        Assertions.assertNull(logger);
    }

}
